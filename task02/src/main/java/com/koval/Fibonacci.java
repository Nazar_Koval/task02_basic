package com.koval;

import java.util.Vector;

/**
 * Helping class with all necessary methods
 */

public class Fibonacci {

    /**
     *  Function to generate Fibonacci sequence
     * @param N Const variable - amount of Fibonacci numbers to generate
     * @return An array with all the generated numbers
     */
    public static int [] FibonacciSequence(final int N){
        int[] fib = new int[N];
        fib[0] = 0;
        fib[1] = 1;

        for(int i = 2; i < N; i++){
            fib[i] = fib[i-1] + fib[i-2];
        }

        return fib;
    }

    /**
     * Function to get all odd numbers from an interval
     * @param start Const variable - first number from the interval
     * @param end Const variable - last number from the interval
     * @return A vector with all odd numbers
     */
     public static Vector<Integer> OddNumbers( final int start, final int end){
        Vector<Integer>result = new Vector<Integer>();

        for(int i = start; i <= end; i++){

            if(i % 2 != 0){
                result.add(i);
            }

        }

        return result;
    }

    /**
     * Function to calculate the sum of the numbers generated from OddNumbers function
     * @param integers Returned variable from OddNumbers function
     * @return Sum of all the elements of a given vector
     */
     public static int OddSum(Vector<Integer>integers){
        int ans = 0;

        for (Integer i: integers) {
            ans += i;
            ans += i;
        }

        return ans;
    }

    /**
     * Function that returns the biggest even Fibonacci number from a given sequence
     * @param fib Const variable - generated array from FibonacciSequence function
     * @param N Const variable - amount of numbers in the sequence
     * @return the biggest even Fibonacci number, stored in the ans variable
     */
     public static int BiggestEvenFibonacci(final int[] fib, final int N){
        int ans = 0;

        for(int i = N-1; i >= 0; i--){

            if(fib[i] % 2 == 0){
                ans = fib[i];
                break;
            }

        }

        return ans;
    }

    /**
     * Function that returns the biggest odd Fibonacci number from a given sequence.
     * @param fib Const variable - generated array from FibonacciSequence function
     * @param N Const variable - amount of numbers in the sequence
     * @return the biggest odd Fibonacci number, stored in the ans variable
     */
     public static  int BiggestOddFibonacci(final int[] fib, final int N){
        int ans = 0;

        for(int i = N-1; i >= 0; i--){

            if(fib[i] % 2 != 0){
                ans = fib[i];
                break;
            }

        }

        return ans;
    }

    /**
     * Function that prints percentages of both odd and even Fibonacci numbers in the given sequence.
     * Variable odd - variable to store the amount of odd Fibonacci numbers.
     * Variable even - variable to store the amount of even Fibonacci numbers.
     * @param fib Const variable - generated array from FibonacciSequence function
     * @param N Const variable - amount of numbers in the sequence
     */
     public static void FibonacciPercentage(final int[] fib, final int N){
        int odd = 0;
        int even = 0;

        for (int i = 0; i < N; i++) {

            if(fib[i] % 2 == 0){
                odd++;
            }else {
                even++;
            }

        }

        int odd_p = (odd*100)/N;
        int even_p = (even*100)/N;
        System.out.print("Fibonacci numbers` percentage(odd/even) : " + odd_p + "%" + "/" + even_p + "%");

    }
}
