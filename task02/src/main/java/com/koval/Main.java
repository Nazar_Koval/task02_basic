package com.koval;

import java.util.Scanner;
import java.util.Vector;

/**
 * Main class that extends helping class Fibonacci and uses all the methods from there.
 * In the function main all methods from Fibonacci class will be called.
 */

public class Main extends Fibonacci {

    /**
     * Main function where all the methods from Fibonacci class are called.
     * @param args Command line`s arguments
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); /*Scanner for user`s input */

        System.out.println("Input N");
        int N = scanner.nextInt(); /* Input data */

        /* Fibonacci sequence`s of N length generation */
        final int[]fib = FibonacciSequence(N);/* Resulting array */

        /* Generation of all the odd numbers from 1 to 100 */
         System.out.print("Input start point");
         int start = scanner.nextInt(); /* Start point */
         System.out.println("Input end point");
         int end = scanner.nextInt(); /* End point */

        Vector<Integer>oddVector = OddNumbers(start,end); /* Resulting vector */
        System.out.println("Result of the function");
        for(Integer i : oddVector){ /* Output of the odd numbers */
            System.out.print(i + " ");
        }

        System.out.println("Sum of all the numbers in the vector");
        int sum = OddSum(oddVector); /* Sum of the elements from oddVector */
        System.out.println(sum);

        long BiggestEven = BiggestEvenFibonacci(fib,N); /* Biggest even Fibonacci number from the fib array */
        System.out.println("Biggest even Fibonacci number");
        System.out.println(BiggestEven);

        long BiggestOdd = BiggestOddFibonacci(fib,N); /* Biggest odd Fibonacci number from the fib array */
        System.out.println("Biggest odd Fibonacci number");
        System.out.println(BiggestOdd);

        /* Output of the percentage of odd and even numbers from the fib array */
        FibonacciPercentage(fib,N);

    }
}
